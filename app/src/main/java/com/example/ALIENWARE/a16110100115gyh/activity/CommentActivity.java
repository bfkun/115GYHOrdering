package com.example.ALIENWARE.a16110100115gyh.activity;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.adapter.CommentAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.BaseActivity;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.bean.CommentBean;
import com.example.ALIENWARE.a16110100115gyh.model.CommentModel;
import com.example.ALIENWARE.a16110100115gyh.util.SharedPreferencesUtils;

import java.util.List;

public class CommentActivity extends BaseActivity {

    private RecyclerView recyclerView;
    Context context;
    private CommentAdapter commentAdapter;
    LinearLayoutManager layoutManager;
    private List<CommentBean> beanList;
    private SharedPreferencesUtils sharedPreferencesUtils;
    private Integer user_id;
    @Override
    public int getLayoutFile() {
        return R.layout.activity_comment;
    }

    @Override
    public void initView() {
        context = getContext();
        recyclerView = (RecyclerView)findViewById(R.id.rv_comment);
        sharedPreferencesUtils = new SharedPreferencesUtils(getContext());
    }

    @Override
    public void initEvent() {

        commentAdapter = new CommentAdapter(getContext());
        recyclerView.setAdapter(commentAdapter);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);    //提高item性能
        recyclerView.addItemDecoration(new DividerItemDecoration(context,DividerItemDecoration.VERTICAL));
    }

    @Override
    public void initData() {
        user_id = Integer.parseInt(sharedPreferencesUtils.readString("userid"));
        CommentModel commentModel = new CommentModel();
        commentModel.getAllComments(user_id , baseListener);
    }

    @Override
    public void onClick(View view) {

    }

    BaseListener<List<CommentBean>> baseListener = new BaseListener<List<CommentBean>>() {
        @Override
        public void onResponse(List<CommentBean> commentBeans) {
            if (null != commentBeans){
                beanList = commentBeans;
                commentAdapter.setList(beanList);
            }else {
                showToast("kong");
            }
        }

        @Override
        public void onFail(String msg) {
            showToast(msg);
        }
    };
}
