package com.example.ALIENWARE.a16110100115gyh.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseActivity;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.base.URL;
import com.example.ALIENWARE.a16110100115gyh.bean.IscollectBean;
import com.example.ALIENWARE.a16110100115gyh.bean.UsercollectfoodBean;
import com.example.ALIENWARE.a16110100115gyh.bean.UsercollectshopBean;
import com.example.ALIENWARE.a16110100115gyh.util.SharedPreferencesUtils;
import com.squareup.picasso.Picasso;

import retrofit2.Call;

public class FoodbyidActivity extends BaseActivity {

    private ImageView iv_foodbyid_pic,iv_foodbyid_start,iv_collect_shop_xin;
    private TextView tv_foodbyid_jiage2,tv_foodbyid_intro,tv_foodbyid_name;
    private Button bt_foodbyid_gouwu,bt_foodbyid_zhijie;
    private String pic,intro,foodname;
    private double price;
    private Integer foodid,userid,shipid;
    private boolean isCollect , onCollect;
    private SharedPreferencesUtils sharedPreferencesUtils;
    private Context context;

    @Override
    public int getLayoutFile() {
        return R.layout.activity_foodbyid;
    }

    @Override
    public void initView() {
        iv_foodbyid_pic = (ImageView)findViewById(R.id.iv_foodbyid_pic);
        iv_foodbyid_start = (ImageView)findViewById(R.id.iv_foodbyid_start);
        iv_collect_shop_xin = (ImageView)findViewById(R.id.iv_collect_shop_xin);
        tv_foodbyid_intro = (TextView)findViewById(R.id.tv_foodbyid_intro);
        tv_foodbyid_name = (TextView)findViewById(R.id.tv_foodbyid_name);
        tv_foodbyid_jiage2 = (TextView)findViewById(R.id.tv_foodbyid_jiage2);
        bt_foodbyid_gouwu = (Button)findViewById(R.id.bt_foodbyid_gouwu);
        bt_foodbyid_zhijie = (Button)findViewById(R.id.bt_foodbyid_zhijie);

        iv_foodbyid_start.setOnClickListener(this);
        iv_collect_shop_xin.setOnClickListener(this);
        bt_foodbyid_zhijie.setOnClickListener(this);
        bt_foodbyid_gouwu.setOnClickListener(this);
    }

    @Override
    public void initEvent() {
        context = getApplicationContext();
    }

    @Override
    public void initData() {
        sharedPreferencesUtils = new SharedPreferencesUtils(context);
        userid = Integer.parseInt(sharedPreferencesUtils.readString("userid"));
        Intent intent2 = getIntent();
        pic = intent2.getStringExtra("pic");
        intro = intent2.getStringExtra("intro");
        foodname = intent2.getStringExtra("foodname");
        price = intent2.getDoubleExtra("price",1.0);
        foodid = intent2.getIntExtra("foodid",0);
        shipid = intent2.getIntExtra("shopid",0);

        Picasso.get().load(URL.BASEUSRL + pic).into(iv_foodbyid_pic);
        tv_foodbyid_intro.setText(intro);
        tv_foodbyid_jiage2.setText(price+"");
        tv_foodbyid_name.setText(foodname);

        isCollect();
        onCollect();
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_foodbyid_start:
                doCollect();
                break;
            case R.id.iv_collect_shop_xin:
                doesCollect();
                break;
            case R.id.bt_foodbyid_gouwu:
                showToast("加入购物车成功");
                break;
            case R.id.bt_foodbyid_zhijie:
                showToast("购买成功");
                break;
            default:
                break;
        }
    }

    //判断是否收藏(菜谱)
    private void isCollect(){
        BaseModel<IscollectBean> model = new BaseModel<>();
        Call<IscollectBean> call = model.service.getiscollect(userid,foodid,1);
        model.callEnqueue(call, new BaseListener<IscollectBean>() {
            @Override
            public void onResponse(IscollectBean iscollectBean) {
                if (iscollectBean.getCollected().equals("0")){
                    isCollect = false;
                    iv_foodbyid_start.setImageResource(R.drawable.ic_star_border);
                }
                else {
                    isCollect = true;
                    iv_foodbyid_start.setImageResource(R.drawable.ic_star_yellow);
                }
            }

            @Override
            public void onFail(String msg) {

                showToast(msg);
            }
        });
    }

    //收藏/取消收藏(菜谱)
    private void doCollect(){
        BaseModel<UsercollectfoodBean> baseModel = new BaseModel<>();
        Call<UsercollectfoodBean> call = baseModel.service.getusercollectfood(userid,foodid);
        baseModel.callEnqueue(call, new BaseListener<UsercollectfoodBean>() {
            @Override
            public void onResponse(UsercollectfoodBean usercollectfoodBean) {
                if (usercollectfoodBean.getSuccess().equals("0")){
                    showToast("收藏失败");
                }
                else {
                    if (isCollect){
                        isCollect = false;
                        iv_foodbyid_start.setImageResource(R.drawable.ic_star_border);
                        showToast("取消收藏成功");
                    }
                    else {
                        isCollect = true;
                        iv_foodbyid_start.setImageResource(R.drawable.ic_star_yellow);
                        showToast("收藏成功");
                    }
                }
            }

            @Override
            public void onFail(String msg) {

                showToast(msg);
            }
        });
    }

    //判断是否收藏(店铺)
    private void onCollect(){
        BaseModel<IscollectBean> model = new BaseModel<>();
        Call<IscollectBean> call = model.service.getiscollect(userid,shipid,0);
        model.callEnqueue(call, new BaseListener<IscollectBean>() {
            @Override
            public void onResponse(IscollectBean iscollectBean) {
                if (iscollectBean.getCollected().equals("0")){
                    onCollect = false;
                    iv_collect_shop_xin.setImageResource(R.drawable.ic_xin_border);
                }
                else {
                    onCollect = true;
                    iv_collect_shop_xin.setImageResource(R.drawable.ic_xin_red);
                }
            }

            @Override
            public void onFail(String msg) {

                showToast(msg);
            }
        });
    }

    //收藏/取消收藏(店铺)
    private void doesCollect(){
        BaseModel<UsercollectshopBean> baseModel = new BaseModel<>();
        Call<UsercollectshopBean> call = baseModel.service.getusercollectshop(userid,shipid);
        baseModel.callEnqueue(call, new BaseListener<UsercollectshopBean>() {
            @Override
            public void onResponse(UsercollectshopBean usercollectshopBean) {
                if (usercollectshopBean.getSuccess().equals("0")){
                    showToast("收藏失败");
                }
                else {
                    if (onCollect){
                        onCollect = false;
                        iv_collect_shop_xin.setImageResource(R.drawable.ic_xin_border);
                        showToast("取消收藏成功");
                    }
                    else {
                        onCollect = true;
                        iv_collect_shop_xin.setImageResource(R.drawable.ic_xin_red);
                        showToast("收藏成功");
                    }
                }
            }

            @Override
            public void onFail(String msg) {

                showToast(msg);
            }
        });
    }
}
