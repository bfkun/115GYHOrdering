package com.example.ALIENWARE.a16110100115gyh.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.adapter.FoodbyshopAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.BaseActivity;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.bean.FoodbyshopBean;
import com.example.ALIENWARE.a16110100115gyh.model.FoodbyshopModel;

import java.util.List;

public class FoodbyshopActivity extends BaseActivity {

    private RecyclerView recyclerView;
    Context context;
    private FoodbyshopAdapter foodbyshopAdapter;
    LinearLayoutManager layoutManager;
    private List<FoodbyshopBean> beanList;
    FoodbyshopAdapter.OnItemClickListener onItemClickListener;

    @Override
    public int getLayoutFile() {
        return R.layout.activity_foodbyshop;
    }

    @Override
    public void initView() {
        recyclerView = (RecyclerView)findViewById(R.id.foodbushop_re);
        context = getApplication();
    }

    @Override
    public void initEvent() {
        foodbyshopAdapter = new FoodbyshopAdapter(context);
        recyclerView.setAdapter(foodbyshopAdapter);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);    //提高item性能
        recyclerView.addItemDecoration(new DividerItemDecoration(context,DividerItemDecoration.VERTICAL));

    }

    @Override
    public void initData() {
        final Intent intent = getIntent();
        int shop_id2;
        shop_id2 = intent.getIntExtra("shop_id",1);
        Toast.makeText(getApplicationContext(),shop_id2+"", Toast.LENGTH_SHORT).show();

        onItemClickListener = new FoodbyshopAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent2 = new Intent(getApplicationContext(),FoodbyidActivity.class);
                intent2.putExtra("price",beanList.get(position).getPrice());
                intent2.putExtra("foodname",beanList.get(position).getFoodname());
                intent2.putExtra("intro",beanList.get(position).getIntro());
                intent2.putExtra("pic",beanList.get(position).getPic());
                intent2.putExtra("foodid",beanList.get(position).getFood_id());
                intent2.putExtra("shopid",beanList.get(position).getShop_id());
                startActivity(intent2);
            }
        };
        foodbyshopAdapter.setOnItemClickListener(onItemClickListener);

        FoodbyshopModel foodbyshopModel = new FoodbyshopModel();
        foodbyshopModel.getfoodbyshop(shop_id2,foodlistener);
    }

    @Override
    public void onClick(View view) {

    }
    BaseListener<List<FoodbyshopBean>> foodlistener = new BaseListener<List<FoodbyshopBean>>() {
        @Override
        public void onResponse(List<FoodbyshopBean> foodbyshopBeans) {
            beanList=foodbyshopBeans;
            if (beanList==null){
                Toast.makeText(getContext(),"Kong", Toast.LENGTH_LONG).show();

            }

            else{
                foodbyshopAdapter.setList(beanList);

            }
        }

        @Override
        public void onFail(String msg) {
            showToast(msg);
        }
    };
}
