package com.example.ALIENWARE.a16110100115gyh.activity;


import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseActivity;
import com.example.ALIENWARE.a16110100115gyh.fragment.CollectFragment;
import com.example.ALIENWARE.a16110100115gyh.fragment.SearchFragment;
import com.example.ALIENWARE.a16110100115gyh.fragment.ShopFragment;
import com.example.ALIENWARE.a16110100115gyh.fragment.UserInfoFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener,ViewPager.OnPageChangeListener{

    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;
    private FragmentPagerAdapter adapter;
    private List<Fragment> fragmentList;


    @Override
    public int getLayoutFile() {
        return R.layout.activity_home;
    }

    @Override
    public void initView() {
        viewPager=(ViewPager) findViewById(R.id.viewPager);
        bottomNavigationView=(BottomNavigationView)findViewById(R.id.bottomNavigationView);
    }

    @Override
    public void initEvent() {
        initfragment();

        adapter=new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }
        };
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setOffscreenPageLimit(4);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setSelectedItemId(R.id.nav_shop);
    }

    private void initfragment() {
        fragmentList=new ArrayList<>();
        fragmentList.add(new ShopFragment());
        fragmentList.add(new CollectFragment());
        fragmentList.add(new SearchFragment());
        fragmentList.add(new UserInfoFragment());
    }

    @Override
    public void initData() {
//        Intent intent =getIntent();
//        String data = "用户名:"+intent.getStringExtra("yonghuming")+" "+"密码:"+intent.getStringExtra("mima");
//        showToast(data);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id=0;
        switch (item.getItemId()){
            case R.id.nav_shop:
                id=0;
                break;
            case R.id. nav_collection:
                id=1;
                break;
            case R.id.nav_search:
                id=2;
                break;
            case R.id.nav_user:
                id=3;
                break;

        }
        viewPager.setCurrentItem(id);

        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        int id=0;
        switch (position){
            case 0:
                id=R.id.nav_shop;
                break;
            case 1:
                id=R.id.nav_collection;
                break;
            case 2:
                id=R.id.nav_search;
                break;
            case 3:
                id=R.id.nav_user;
                break;

        }
        bottomNavigationView.setSelectedItemId(id);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
