package com.example.ALIENWARE.a16110100115gyh.activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.bean.LoginBean;
import com.example.ALIENWARE.a16110100115gyh.model.LoginModel;
import com.example.ALIENWARE.a16110100115gyh.util.FullScreen;
import com.example.ALIENWARE.a16110100115gyh.util.SharedPreferencesUtils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private Button bt_login_denglu;
    private EditText et_login_yonghuming,et_login_mima;
    private TextView tv_login_xinyonghu;
    private SharedPreferencesUtils sharedPreferencesUtils;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FullScreen.fullScreen(this,0x33131313);
        setContentView(R.layout.activity_login);
        bt_login_denglu = (Button)findViewById(R.id.bt_login_denglu);
        bt_login_denglu.setOnClickListener(this);
        et_login_yonghuming = (EditText)findViewById(R.id.et_login_yonghuming);
        et_login_mima = (EditText)findViewById(R.id.et_login_mima);
        tv_login_xinyonghu = (TextView)findViewById(R.id.tv_login_xinyonghu);
        tv_login_xinyonghu.setOnClickListener(this);
        context = getApplicationContext();
        sharedPreferencesUtils = new SharedPreferencesUtils(context);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_login_xinyonghu:
                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
                break;
            case R.id.bt_login_denglu:
                String yonghuming = et_login_yonghuming.getText().toString();
                String mima = et_login_mima.getText().toString();
                LoginModel loginModel = new LoginModel();
                loginModel.login(yonghuming,mima,loginlistener);

                finish();

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case 1:{
                if(resultCode==RESULT_OK){
                    et_login_yonghuming.setText(data.getStringExtra("usernmae_data"));
                }
                break;
            }
            default:break;
        }
    }

    BaseListener<LoginBean> loginlistener = new BaseListener<LoginBean>() {
        @Override
        public void onResponse(LoginBean loginBean) {
            if (loginBean.getUserid().equals("0")){
                Toast.makeText(getApplicationContext(),"登录失败", Toast.LENGTH_SHORT).show();
            }
            else {
                sharedPreferencesUtils.saveString("userid",loginBean.getUserid());
                Intent intent1 = new Intent(LoginActivity.this,HomeActivity.class);
                startActivity(intent1);
            }
        }

        @Override
        public void onFail(String msg) {
            Toast.makeText(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
        }
    };
}
