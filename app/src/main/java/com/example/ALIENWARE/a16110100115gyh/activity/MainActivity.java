package com.example.ALIENWARE.a16110100115gyh.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.bean.RegisterBean;
import com.example.ALIENWARE.a16110100115gyh.model.RegisterModel;
import com.example.ALIENWARE.a16110100115gyh.util.FullScreen;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button bt_register;
    private EditText et_register_yonghuming,et_register_mima,et_register_address,et_register_phone,et_register_beizhu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FullScreen.fullScreen(this,0x33131313);
        setContentView(R.layout.activity_main);
        initview();
    }
    private void initview() {
        bt_register = (Button)findViewById(R.id.bt_register);
        et_register_yonghuming = (EditText)findViewById(R.id.et_register_yonghuming);
        et_register_mima = (EditText)findViewById(R.id.et_register_mima);
        et_register_phone = (EditText)findViewById(R.id.et_register_phone);
        et_register_address = (EditText)findViewById(R.id.et_register_address);
        et_register_beizhu = (EditText)findViewById(R.id.et_register_beizhu);
        bt_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_register:
                String yonghuming = et_register_yonghuming.getText().toString();
                String mima = et_register_mima.getText().toString();
                String phone = et_register_phone.getText().toString();
                String address = et_register_address.getText().toString();
                String beizhu = et_register_beizhu.getText().toString();

                if (TextUtils.isEmpty(yonghuming)){
                    Toast.makeText(getApplicationContext(),"用户名为空", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(mima)){
                    Toast.makeText(getApplicationContext(),"密码为空", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(phone)){
                    Toast.makeText(getApplicationContext(),"手机号码为空", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(address)){
                    Toast.makeText(getApplicationContext(),"地址为空", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(beizhu)){
                    Toast.makeText(getApplicationContext(),"备注为空", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(com.example.ALIENWARE.a16110100115gyh.activity.MainActivity.this,LoginActivity.class);
                    RegisterModel registerModel = new RegisterModel();
                    registerModel.register(yonghuming,mima,phone,address,beizhu,registerlistener);

                    startActivity(intent);
                }

        }
    }

    BaseListener<RegisterBean> registerlistener = new BaseListener<RegisterBean>() {
        @Override
        public void onResponse(RegisterBean registerBean) {
            if (registerBean.getSuccess().equals("0")){
                Toast.makeText(getApplicationContext(),"注册失败", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getApplicationContext(),"注册成功", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFail(String msg) {
            Toast.makeText(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
        }
    };
}
