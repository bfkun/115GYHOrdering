package com.example.ALIENWARE.a16110100115gyh.activity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseActivity;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.bean.UpdateuserBean;
import com.example.ALIENWARE.a16110100115gyh.bean.UserbyidBean;
import com.example.ALIENWARE.a16110100115gyh.model.UpdateModel;
import com.example.ALIENWARE.a16110100115gyh.model.UserModel;
import com.example.ALIENWARE.a16110100115gyh.util.SharedPreferencesUtils;

public class UpdateuserActivity extends BaseActivity {

    private EditText et_update_username , et_update_userpass , et_update_mobile , et_update_address;
    private Button bt_update;
    private Integer user_id;
    private SharedPreferencesUtils sharedPreferencesUtils;
    private UserbyidBean beans;

    @Override
    public int getLayoutFile() {
        return R.layout.activity_updateuser;
    }

    @Override
    public void initView() {

        et_update_username = (EditText)findViewById(R.id.et_update_username);
        et_update_userpass = (EditText)findViewById(R.id.et_update_userpass);
        et_update_mobile = (EditText)findViewById(R.id.et_update_mobile);
        et_update_address = (EditText)findViewById(R.id.et_update_address);
        bt_update = (Button)findViewById(R.id.bt_update);
        bt_update.setOnClickListener(this);
    }

    @Override
    public void initEvent() {
        sharedPreferencesUtils = new SharedPreferencesUtils(getContext());
        user_id = Integer.parseInt(sharedPreferencesUtils.readString("userid"));
        UserModel userModel = new UserModel();
        userModel.getuserbyid(user_id , baseListener);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_update:
                UpdateModel updateModel = new UpdateModel();
                updateModel.getupdateuser(user_id , et_update_username.getText().toString() , et_update_userpass.getText().toString(),
                        et_update_mobile.getText().toString() , et_update_address.getText().toString() , listener);
                break;
        }
    }

    BaseListener<UserbyidBean> baseListener = new BaseListener<UserbyidBean>() {
        @Override
        public void onResponse(UserbyidBean userbyidBean) {

            if (null != userbyidBean){
                beans = userbyidBean;
                et_update_username.setText(beans.getUsername());
                et_update_userpass.setText(beans.getUserpass());
                et_update_address.setText(beans.getAddress());
                et_update_mobile.setText(beans.getMobilenum());
            }
        }

        @Override
        public void onFail(String msg) {
            showToast(msg);
        }
    };

    BaseListener<UpdateuserBean> listener = new BaseListener<UpdateuserBean>() {
        @Override
        public void onResponse(UpdateuserBean updateuserBean) {
            if (null != updateuserBean){
                if (updateuserBean.getSuccess().equals("1")){
                    showToast("修改成功");
                    finish();
                }
                else {
                    showToast("修改失败");
                }
            }
        }

        @Override
        public void onFail(String msg) {
            showToast(msg);
        }
    };
}
