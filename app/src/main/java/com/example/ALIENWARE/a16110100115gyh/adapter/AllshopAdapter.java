package com.example.ALIENWARE.a16110100115gyh.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.URL;
import com.example.ALIENWARE.a16110100115gyh.bean.AllshopBean;
import com.squareup.picasso.Picasso;


public class AllshopAdapter extends BaseAdapter<AllshopBean> {

    AllshopAdapter.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }
    public void setOnItemClickListener(AllshopAdapter.OnItemClickListener listener){
        onItemClickListener= listener;
    }

    public AllshopAdapter(Context context) {
        super(context);
        layoutInflater= LayoutInflater.from(context);
    }

    public class AllshopViewHolder extends RecyclerView.ViewHolder{

        private ImageView iv_allshop_pic;
        private TextView tv_allshop_shopname,tv_allshop_address;

        public AllshopViewHolder(View itemView) {
            super(itemView);
            iv_allshop_pic = itemView.findViewById(R.id.iv_shopall_pic);
            tv_allshop_shopname = itemView.findViewById(R.id.tv_allshop_shopname);
            tv_allshop_address = itemView.findViewById(R.id.tv_allshop_address);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=layoutInflater.inflate(R.layout.item_allshop,parent,false);
        return new AllshopAdapter.AllshopViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final AllshopBean allshopBean = list.get(position);
        final AllshopAdapter.AllshopViewHolder viewHolder = (AllshopAdapter.AllshopViewHolder)holder;
        final int mPostion = position;
        viewHolder.tv_allshop_shopname.setText(allshopBean.getShopname());
        viewHolder.tv_allshop_address.setText(allshopBean.getAddress());
        Picasso.get().
                load(URL.BASEUSRL + allshopBean.getPic()).
                into(viewHolder.iv_allshop_pic);
        if (onItemClickListener!=null){
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(view,mPostion);
                }
            });
        }
    }

}
