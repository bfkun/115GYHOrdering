package com.example.ALIENWARE.a16110100115gyh.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.URL;
import com.example.ALIENWARE.a16110100115gyh.bean.UsercollectBean;
import com.squareup.picasso.Picasso;

public class CollectionFoodAdapter extends BaseAdapter<UsercollectBean> {

    CollectionFoodAdapter.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }
    public void setOnItemClickListener(CollectionFoodAdapter.OnItemClickListener listener){
        this.onItemClickListener= listener;
    }
    public CollectionFoodAdapter(Context context) {
        super(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView iv_collect_food_pic;
        private TextView tv_collect_foodname , tv_collect_food_price , tv_collect_food_address;
        public ViewHolder(View itemView) {
            super(itemView);
            iv_collect_food_pic = itemView.findViewById(R.id.iv_collect_food_pic);
            tv_collect_foodname = itemView.findViewById(R.id.tv_collect_foodname);
            tv_collect_food_price = itemView.findViewById(R.id.tv_collect_food_price);
            tv_collect_food_address = itemView.findViewById(R.id.tv_collect_food_address);
        }
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=layoutInflater.inflate(R.layout.item_collect_food,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final UsercollectBean bean = list.get(position);
        final ViewHolder viewHolder = (ViewHolder)holder;
        final int mPosition = position;
        viewHolder.tv_collect_foodname.setText( bean.getFoodname() + "");
        viewHolder.tv_collect_food_price.setText(bean.getPrice() + "");
        viewHolder.tv_collect_food_address.setText(bean.getAddress() + "");
        Picasso.get().
                load(URL.BASEUSRL + bean.getPic()).
                into(viewHolder.iv_collect_food_pic);

        if (onItemClickListener!=null){
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(view,mPosition);
                }
            });
        }
    }
}
