package com.example.ALIENWARE.a16110100115gyh.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.URL;
import com.example.ALIENWARE.a16110100115gyh.bean.UsercollectBean;
import com.squareup.picasso.Picasso;

public class CollectionShopAdapter extends BaseAdapter<UsercollectBean> {

    CollectionShopAdapter.OnItemClickListener onItemClickListener;

    public CollectionShopAdapter(Context context) {
        super(context);
    }
    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }
    public void setOnItemClickListener(CollectionShopAdapter.OnItemClickListener listener){
        this.onItemClickListener= listener;
    }
    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView iv_collect_shop_pic;
        private TextView tv_collect_shopname , tv_collect_shop_address;
        public ViewHolder(View itemView) {
            super(itemView);
            iv_collect_shop_pic = itemView.findViewById(R.id.iv_collect_shop_pic);
            tv_collect_shop_address = itemView.findViewById(R.id.tv_collect_shop_address);
            tv_collect_shopname = itemView.findViewById(R.id.tv_collect_shopname);
        }
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=layoutInflater.inflate(R.layout.item_collect_shop,parent,false);
        return new CollectionShopAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final UsercollectBean bean = list.get(position);
        final CollectionShopAdapter.ViewHolder viewHolder = (CollectionShopAdapter.ViewHolder)holder;
        final int mPosition = position;

        viewHolder.tv_collect_shopname.setText(bean.getShopname() + "");
        viewHolder.tv_collect_shop_address.setText(bean.getAddress() + "");
        Picasso.get().
                load(URL.BASEUSRL + bean.getPic()).
                into(viewHolder.iv_collect_shop_pic);

        if (onItemClickListener!=null){
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(view,mPosition);
                }
            });
        }
    }
}
