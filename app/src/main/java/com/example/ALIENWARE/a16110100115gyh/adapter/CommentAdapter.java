package com.example.ALIENWARE.a16110100115gyh.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseAdapter;
import com.example.ALIENWARE.a16110100115gyh.bean.CommentBean;

public class CommentAdapter extends BaseAdapter<CommentBean> {


    public CommentAdapter(Context context) {
        super(context);
        layoutInflater= LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=layoutInflater.inflate(R.layout.item_food_comment,parent,false);
        return new CommentAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final CommentBean commentBean = list.get(position);
        CommentAdapter.ViewHolder viewHolder = (CommentAdapter.ViewHolder)holder;

        viewHolder.tv_time.setText(commentBean.getComment_time());
        viewHolder.tv_comment.setText(commentBean.getContent());
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_time , tv_comment;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_comment = itemView.findViewById(R.id.tv_comment);
        }
    }
}
