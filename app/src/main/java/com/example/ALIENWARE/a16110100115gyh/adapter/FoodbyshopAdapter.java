package com.example.ALIENWARE.a16110100115gyh.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.URL;
import com.example.ALIENWARE.a16110100115gyh.bean.FoodbyshopBean;
import com.squareup.picasso.Picasso;


public class FoodbyshopAdapter extends BaseAdapter<FoodbyshopBean> {

    FoodbyshopAdapter.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }
    public void setOnItemClickListener(FoodbyshopAdapter.OnItemClickListener listener){
        onItemClickListener= listener;
    }

    public FoodbyshopAdapter(Context context) {
        super(context);
        layoutInflater= LayoutInflater.from(context);
    }

    public class FoodbyshopViewHolder extends RecyclerView.ViewHolder{

        private ImageView iv_foodbyshop_pic;
        private TextView tv_foodbyshop_foodname,tv_foodbyshop_price;
        public FoodbyshopViewHolder(View itemView) {
            super(itemView);
            iv_foodbyshop_pic = itemView.findViewById(R.id.iv_foodbyshop_pic);
            tv_foodbyshop_foodname = itemView.findViewById(R.id.tv_foodbyshop_foodname);
            tv_foodbyshop_price = itemView.findViewById(R.id.tv_foodbyshop_price);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=layoutInflater.inflate(R.layout.item_foodbyshop,parent,false);
        return new FoodbyshopAdapter.FoodbyshopViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final FoodbyshopBean foodbyshopBean = list.get(position);
        final FoodbyshopViewHolder foodbyshopViewHolder = (FoodbyshopAdapter.FoodbyshopViewHolder)holder;
        final int mPostion = position;
        foodbyshopViewHolder.tv_foodbyshop_foodname.setText(foodbyshopBean.getFoodname());
        foodbyshopViewHolder.tv_foodbyshop_price.setText( foodbyshopBean.getPrice()+"");
        Picasso.get().
                load(URL.BASEUSRL +foodbyshopBean.getPic()).
                into(foodbyshopViewHolder.iv_foodbyshop_pic);

        if (onItemClickListener!=null){
            foodbyshopViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(view,mPostion);
                }
            });
        }
    }
}
