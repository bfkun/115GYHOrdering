package com.example.ALIENWARE.a16110100115gyh.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseAdapter;
import com.example.ALIENWARE.a16110100115gyh.bean.CommentBean;

public class OrderAdapter extends BaseAdapter<CommentBean> {


    public OrderAdapter(Context context) {
        super(context);
        layoutInflater= LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=layoutInflater.inflate(R.layout.item_order,parent,false);
        return new OrderAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final CommentBean commentBean = list.get(position);
        final OrderAdapter.ViewHolder viewHolder= (OrderAdapter.ViewHolder)holder;
        viewHolder.tv_order_foodname.setText(commentBean.getFoodname());
        viewHolder.tv_order_shopname.setText(commentBean.getShopname());
        viewHolder.tv_order_price.setText(commentBean.getPrice() + "元");
        viewHolder.tv_order_time.setText(commentBean.getComment_time());
        viewHolder.tv_order_ordernum.setText(commentBean.getNum() + "");
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_order_foodname , tv_order_shopname , tv_order_time;
        private TextView tv_order_ordernum , tv_order_price;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_order_foodname = itemView.findViewById(R.id.tv_order_foodname);
            tv_order_shopname = itemView.findViewById(R.id.tv_order_shopname);
            tv_order_ordernum = itemView.findViewById(R.id.tv_order_ordernum);
            tv_order_time = itemView.findViewById(R.id.tv_order_time);
            tv_order_price = itemView.findViewById(R.id.tv_order_price);
        }
    }
}
