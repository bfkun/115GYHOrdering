package com.example.ALIENWARE.a16110100115gyh.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.URL;
import com.example.ALIENWARE.a16110100115gyh.bean.FoodbysearchBean;
import com.squareup.picasso.Picasso;

public class SearchAdapter extends BaseAdapter<FoodbysearchBean> {


    public SearchAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=layoutInflater.inflate(R.layout.item_re,parent,false);
        return new SearchAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final FoodbysearchBean searchbean = list.get(position);
        final SearchAdapter.ViewHolder viewHolder = (SearchAdapter.ViewHolder)holder;
        final int mPostion = position;
        viewHolder.tv_item1.setText(searchbean.getFoodname());
        viewHolder.tv_item2.setText(searchbean.getIntro());
        Picasso.get().
                load(URL.BASEUSRL + searchbean.getPic()).
                into(viewHolder.iv_search_food);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_item1 , tv_item2;
        private ImageView iv_search_food;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_item1 = itemView.findViewById(R.id.tv_item1);
            tv_item2 = itemView.findViewById(R.id.tv_item2);
            iv_search_food = itemView.findViewById(R.id.iv_search_food);
        }
    }
}
