package com.example.ALIENWARE.a16110100115gyh.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.gyf.barlibrary.ImmersionBar;




public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    public Intent intent;
    public ImmersionBar immersionBar;

    /**
     * 加载布局
     */
    abstract public int getLayoutFile();

    /**
     * 初始化控件(findViewById)
     */
    abstract public void initView();

    /**
     * 初始化事件(setEvent)
     */
    abstract public void initEvent();

    /**
     * 初始化数据(setEvent)
     */
    abstract public void initData();

    /**
     * 得到intent对象
     */
    public void initOnter() {
        intent = getIntent();
    }

    /**
     * 创建Activity和初始化
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutFile());
        showLog("");
        initView();
        initEvent();
        initData();
        initOnter();
    }

    public Context getContext() {
        return this;
    }

    /**
     * 显示Toast
     */
    public final void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Log
     */
    public void showLog(String text){
        Log.d(getClass().getName(),text);
    }

    /**
     * [页面跳转]
     *
     * @param clz
     */
    public void startActivity(Class<?> clz) {
        startActivity(clz, null);
    }

    /**
     * [携带数据的页面跳转]
     *
     * @param clz
     * @param bundle
     */
    public void startActivity(Class<?> clz, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(this, clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * [含有Bundle通过Class打开编辑界面]
     *
     * @param cls
     * @param bundle
     * @param requestCode
     */
    public void startActivityForResult(Class<?> cls, Bundle bundle, int requestCode) {
        Intent intent = new Intent();
        intent.setClass(this, cls);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, requestCode);
    }

    /**
     * 初始化状态栏
     */
    private void initImmersionBar() {
        if (immersionBar == null) {
            immersionBar = ImmersionBar.with(this);
            immersionBar
                    .keyboardEnable(true)
                    .navigationBarWithKitkatEnable(false)
                    .init();
        }
    }

    /**
     * 详情见下一方法
     */
    protected void initImmersionBarForTopBar(View topBar) {
        initImmersionBarForTopBar(topBar, true);
    }

    /**
     * 初始化状态栏:可拉伸图片状态栏
     * 使用“延伸TopBar”方案
     * @param topBar 顶部View
     * @param isStatusBarDarkFont 是否使用深色字体
     */
    protected void initImmersionBarForTopBar(View topBar, boolean isStatusBarDarkFont) {
        initImmersionBar();
        float statusAlpha = isStatusBarDarkFont ? 0.2f : 0;
        immersionBar
                .titleBar(topBar)
                .statusBarDarkFont(isStatusBarDarkFont, statusAlpha)
                .init();
    }

    /**
     * 初始化状态栏:纯色状态栏
     * 使用“改变状态栏颜色”方案
     * @param statusBarColor 状态栏颜色
     * @param isStatusBarDarkFont 是否使用深色字体
     */
    protected void initImmersionBarOfColorBar(@ColorRes int statusBarColor, boolean isStatusBarDarkFont) {
        initImmersionBar();
        immersionBar
                .fitsSystemWindows(true)
                .statusBarColor(statusBarColor)
                .statusBarDarkFont(isStatusBarDarkFont, 0.2f)
                .init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (immersionBar != null) {
            immersionBar.destroy();
        }
    }

    /**
     * 隐藏软键盘
     */
    public static void hideSoftKeyBoard(Activity activity) {
        if (activity == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && imm.isActive()) {
            imm.hideSoftInputFromWindow(activity.getWindow().peekDecorView()
                    .getWindowToken(), 0);
        }
    }

    /**
     * 显示软键盘
     */
    public static void showSoftKeyBoard(Activity activity) {
        if (activity == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && imm.isActive()) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.anim_activity_left_in_slow, R.anim.anim_activity_right_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 获取R.string内容
     * @param id R.string的id
     */
    @NonNull
    public String getStringText(@StringRes int id) throws Resources.NotFoundException {
        return getContext().getResources().getString(id);
    }

    /**
     * 获取R.string内容
     * @param id R.string的id
     * @param formatArgs string的参数列表
     */
    @NonNull
    public String getStringText(@StringRes int id, Object... formatArgs) throws Resources.NotFoundException {
        return getContext().getResources().getString(id,formatArgs);
    }

}