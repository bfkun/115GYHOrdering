package com.example.ALIENWARE.a16110100115gyh.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ALIENWARE.a16110100115gyh.R;

import java.util.List;

public abstract class BaseAdapter<T> extends RecyclerView.Adapter {
    public Context context;
    public LayoutInflater layoutInflater;
    public List<T> list;

    /*
     * 传入上下文
     * 动态加载布局
     */
    public BaseAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    /*
     * 设置数据
     */
    public void setList(List<T> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    /*
     * 追加设置数据
     */
    public void appendList(List<T> newList){
        if(list!=null&&newList!=null) {
            int oldList = list.size();
            list.addAll(newList);
            notifyItemRangeInserted(oldList, newList.size());
        }
    }

    @NonNull
    @Override
    abstract public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType);

    /*
     * 获取数据个数
     */
    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    /*
     * 获取item的位置
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * 获取item的位置
     */
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Toast的简化
     */
    public void showToast(String text){
        Toast.makeText(context,text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Log
     */
    public void showLog(String text){
        Log.d(getClass().getName(),text);
    }

    /**
     * [页面跳转]
     *
     * @param clz
     */
    public void startActivity(Class<?> clz) {
        startActivity(clz, null);
    }

    /**
     * [携带数据的页面跳转]
     *
     * @param clz
     * @param bundle
     */
    public void startActivity(Class<?> clz, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(context, clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }

    public void finish() {
        ((Activity)context).finish();
        ((Activity)context).overridePendingTransition(R.anim.anim_activity_left_in_slow, R.anim.anim_activity_right_out);
    }

    /**
     * 获取R.string内容
     * @param id R.string的id
     */
    @NonNull
    public String getStringText(@StringRes int id) throws Resources.NotFoundException {
        return context.getResources().getString(id);
    }

    /**
     * 获取R.string内容
     * @param id R.string的id
     * @param formatArgs string的参数列表
     */
    @NonNull
    public String getStringText(@StringRes int id, Object... formatArgs) throws Resources.NotFoundException {
        return context.getResources().getString(id,formatArgs);
    }
}
