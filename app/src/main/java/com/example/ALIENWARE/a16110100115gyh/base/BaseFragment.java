package com.example.ALIENWARE.a16110100115gyh.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.gyf.barlibrary.ImmersionBar;




public abstract class BaseFragment extends Fragment implements View.OnClickListener{
    public View view=null;
    public Context context;
    public ImmersionBar immersionBar;

    public abstract @LayoutRes
    int getLayoutFile();

    abstract public void initView(View view);
    abstract public void initEvent();
    abstract public void initData();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(getLayoutFile(), container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initEvent();
        initData();
    }

    /**
     * Toast的简化
     */
    public void showToast(String text){
        Toast.makeText(context,text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Log
     */
    public void showLog(String text){
        Log.d(getClass().getName(),text);
    }

    /**
     * 简化view.findViewById
     */
    public <T extends View> T findViewById(@IdRes int id) {
        return view.findViewById(id);
    }

    public void finish() {
        ((Activity)context).finish();
        ((Activity)context).overridePendingTransition(R.anim.anim_activity_left_in_slow, R.anim.anim_activity_right_out);
    }

//    /**
//     * 初始化状态栏
//     */
//    private void initImmersionBar() {
//        if (immersionBar == null) {
//            immersionBar = ImmersionBar.with(this);
//            immersionBar
//                    .keyboardEnable(true)
//                    .navigationBarWithKitkatEnable(false)
//                    .init();
//        }
//    }
//
//    /**
//     * 详情见下一方法
//     */
//    protected void initImmersionBarForTopBar(View topBar) {
//        initImmersionBarForTopBar(topBar, true);
//    }
//
//    /**
//     * 初始化状态栏:可拉伸图片状态栏
//     * 使用“延伸TopBar”方案
//     * @param topBar 顶部View
//     * @param isStatusBarDarkFont 是否使用深色字体
//     */
//    protected void initImmersionBarForTopBar(View topBar, boolean isStatusBarDarkFont) {
//        initImmersionBar();
//        float statusAlpha = isStatusBarDarkFont ? 0.2f : 0;
//        immersionBar
//                .titleBar(topBar)
//                .statusBarDarkFont(isStatusBarDarkFont, statusAlpha)
//                .init();
//    }
//
//    /**
//     * 初始化状态栏:纯色状态栏
//     * 使用“改变状态栏颜色”方案
//     * @param statusBarColor 状态栏颜色
//     * @param isStatusBarDarkFont 是否使用深色字体
//     */
//    protected void initImmersionBarOfColorBar(@ColorRes int statusBarColor, boolean isStatusBarDarkFont) {
//        initImmersionBar();
//        immersionBar
//                .fitsSystemWindows(true)
//                .statusBarColor(statusBarColor)
//                .statusBarDarkFont(isStatusBarDarkFont, 0.2f)
//                .init();
//    }
//
////    @Override
////    protected void onDestroy() {
////        super.onDestroy();
////        if (immersionBar != null) {
////            immersionBar.destroy();
////        }
////    }
//
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (immersionBar != null){
//            immersionBar.destroy();
//        }
//    }
}
