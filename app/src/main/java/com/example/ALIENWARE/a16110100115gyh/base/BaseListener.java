package com.example.ALIENWARE.a16110100115gyh.base;



public interface BaseListener<T>{
    void onResponse(T t);
    void onFail(String msg);
}
