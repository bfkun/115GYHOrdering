package com.example.ALIENWARE.a16110100115gyh.base;

import com.example.ALIENWARE.a16110100115gyh.bean.AllshopBean;
import com.example.ALIENWARE.a16110100115gyh.bean.CommentBean;
import com.example.ALIENWARE.a16110100115gyh.bean.FoodbyidBean;
import com.example.ALIENWARE.a16110100115gyh.bean.FoodbysearchBean;
import com.example.ALIENWARE.a16110100115gyh.bean.FoodbyshopBean;
import com.example.ALIENWARE.a16110100115gyh.bean.InsertorderBean;
import com.example.ALIENWARE.a16110100115gyh.bean.IscollectBean;
import com.example.ALIENWARE.a16110100115gyh.bean.LoginBean;
import com.example.ALIENWARE.a16110100115gyh.bean.RegisterBean;
import com.example.ALIENWARE.a16110100115gyh.bean.ShopbyidBean;
import com.example.ALIENWARE.a16110100115gyh.bean.UpdateuserBean;
import com.example.ALIENWARE.a16110100115gyh.bean.UserbyidBean;
import com.example.ALIENWARE.a16110100115gyh.bean.UsercollectBean;
import com.example.ALIENWARE.a16110100115gyh.bean.UsercollectfoodBean;
import com.example.ALIENWARE.a16110100115gyh.bean.UsercollectshopBean;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.example.ALIENWARE.a16110100115gyh.base.URL.allshops;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.commentfood;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.foodbyid;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.foodbysearch;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.foodbyshop;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.insertorder;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.iscollect;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.login;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.register;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.shopbyid;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.updateuserid;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.userbyid;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.usercollection;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.usercollectionfood;
import static com.example.ALIENWARE.a16110100115gyh.base.URL.usercollectionshop;


public interface Service {
    //登录   *****
    @GET(login)
    Call<LoginBean> login(@Query("username") String username,
                          @Query("userpass") String userpass);

    //注册   *****
    @GET(register)
    Call<RegisterBean> register(@Query("username") String username,
                                @Query("userpass") String userpass,
                                @Query("mobilenum") String mobileenum,
                                @Query("address") String address,
                                @Query("comment") String comment);
    //店铺列表   *****
    @GET(allshops)
    Call<List<AllshopBean>> getallshop();

    //店铺的所有菜单信息   ******
    @GET(foodbyshop)
    Call<List<FoodbyshopBean>> getfoodbyshop(@Query("shop_id") int shop_id);

    //直接购买
    @GET(insertorder)
    Call<InsertorderBean> getinsertorder(@Query("user_id") int user_id,
                                         @Query("food_id") int food_id,
                                         @Query("num") int num,
                                         @Query("sum") double sum,
                                         @Query("suggesttime") String suggesttime,
                                         @Query("address") String address);

    //店铺详情
    @GET(shopbyid)
    Call<ShopbyidBean> getshopbyid(@Query("shop_id") int shop_id);

    //菜谱详情    ******
    @GET(foodbyid)
    Call<FoodbyidBean> getfoodbyid(@Query("food_id") int food_id);

    //收藏列表   ******
    @GET(usercollection)
    Call<List<UsercollectBean>> getusercollect(@Query("user_id") int user_id,
                                               @Query("flag") int flag);

    //收藏/取消收藏(店铺)   ******
    @GET(usercollectionshop)
    Call<UsercollectshopBean> getusercollectshop(@Query("user_id") int user_id,
                                                 @Query("shop_id") int shop_id);

    //收藏/取消收藏(菜谱)   ******
    @GET(usercollectionfood)
    Call<UsercollectfoodBean> getusercollectfood(@Query("user_id") int user_id,
                                                 @Query("food_id") int food_id);

    //判断是否收藏   ******
    @GET(iscollect)
    Call<IscollectBean> getiscollect(@Query("user_id") int user_id,
                                     @Query("shop_food_id") int shop_food_id,
                                     @Query("flag") int flag);

    //搜索菜谱/口味   *****
    @GET(foodbysearch)
    Call<List<FoodbysearchBean>> getfoodsearch(@Query("search") String search);

    //获取用户信息   *****
    @GET(userbyid)
    Call<UserbyidBean> getuserbyid(@Query("user_id") int user_id);

    //修改用户信息   *****
    @GET(updateuserid)
    Call<UpdateuserBean> getupdateuser(@Query("user_id") int user_id,
                                       @Query("username") String username,
                                       @Query("userpass") String userpass,
                                       @Query("mobilenum") String mobileenum,
                                       @Query("address") String address);

    //获取菜谱评价
    @GET(commentfood)
    Call<List<CommentBean>> getComments(@Query("food_id") int food_id);

    //获取用户所有订单
    @GET("getAllOrdersByUser.do")
    Call<List<CommentBean>> getAllOrders(@Query("user_id") int user_id);

    //获取用户所有评论
    @GET("getAllCommentsByUser.do")
    Call<List<CommentBean>> getAllComments(@Query("user_id") int user_id);
}
