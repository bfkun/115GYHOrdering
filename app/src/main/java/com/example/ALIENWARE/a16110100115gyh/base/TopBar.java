package com.example.ALIENWARE.a16110100115gyh.base;



import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;


/**
 * 用法：
 * <com.lemner.baselib.TopBar
 * android:layout_width="match_parent"
 * android:layout_height="wrap_content"
 * app:titleText="标题"
 * app:btnText="按钮"
 * app:backVisibility="visible"
 * app:titleVisibility="invisible"
 * app:moreVisibility="visible"
 * app:newSrc="@drawable/topbar_new_null"
 * app:btnVisibility="gone"/>
 */

public class TopBar extends LinearLayout {
    private Context context;
    private TextView tv_title;
    private ImageView iv_back, iv_more, iv_new;
    private Button btn_save;
    private View line;

    public TopBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setWillNotDraw(false);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        layoutInflater.inflate(R.layout.topbar, this);
        initView();
        initAttrs(attrs);
        initEvent();
    }

    private void initView() {
        tv_title = findViewById(R.id.tv_title);
        iv_back = findViewById(R.id.iv_back);
        iv_more = findViewById(R.id.iv_more);
        iv_new = findViewById(R.id.iv_new);
        btn_save = findViewById(R.id.btn_save);
        line = findViewById(R.id.line);
    }

    private void initAttrs(AttributeSet attrs) {
        TypedArray a = null;
        try {
            a = context.obtainStyledAttributes(attrs, R.styleable.TopBar);
            int n = a.getIndexCount();
            for (int i = 0; i < n; i++) {
                int attr = a.getIndex(i);
                if (attr == R.styleable.TopBar_titleText) {
                    setText(tv_title, a.getString(attr));
                } else if (attr == R.styleable.TopBar_btnText) {
                    setText(btn_save, a.getString(attr));
                } else if (attr == R.styleable.TopBar_backVisibility) {
                    setVisibility(iv_back, a.getInt(attr, 0));
                } else if (attr == R.styleable.TopBar_titleVisibility) {
                    setVisibility(tv_title, a.getInt(attr, 0));
                } else if (attr == R.styleable.TopBar_moreVisibility) {
                    setVisibility(iv_more, a.getInt(attr, 0));
                } else if (attr == R.styleable.TopBar_newVisibility) {
                    setVisibility(iv_new, a.getInt(attr, 0));
                } else if (attr == R.styleable.TopBar_newSrc) {
                    iv_new.setBackgroundResource(a.getResourceId(attr,0));
                } else if (attr == R.styleable.TopBar_btnVisibility) {
                    setVisibility(btn_save, a.getInt(attr, 0));
                } else if (attr == R.styleable.TopBar_lineVisibility) {
                    setVisibility(line, a.getInt(attr, 0));
                }
            }
        } finally {
            if (null != a) {
                a.recycle();
            }
        }
    }

    private void initEvent() {
        iv_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) context).finish();
            }
        });
    }

    private void setText(TextView tv, CharSequence text) {
        tv.setText(text);
    }

    private void setText(Button btn, CharSequence text) {
        btn.setText(text);
    }

    private void setVisibility(View view, int visibility) {
        switch (visibility) {
            case 0:
                view.setVisibility(View.VISIBLE);
                break;
            case 1:
                view.setVisibility(View.INVISIBLE);
                break;
            case 2:
                view.setVisibility(View.GONE);
                break;
        }
    }
}