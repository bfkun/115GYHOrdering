package com.example.ALIENWARE.a16110100115gyh.base;

public class URL {

    //正式服务器
    public static final String BASEUSRL="http://172.24.10.175:8080/foodService/";

    //登录
    public static final String login = "userLogin.do";

    //注册
    public static final String register = "userRegister.do";

    //店铺列表
    public static final String allshops = "getAllShops.do";

    //店铺的所有菜单信息 getFoodByShop.do
    public static final String foodbyshop = "getFoodByShop.do";

    //直接购买  insertOrder.do
    public static final String insertorder = "insertOrder.do";

    //店铺详情  getShopById.do
    public static final String shopbyid = "getShopById.do";

    //菜谱详情  getFoodById.do
    public static final String foodbyid = "getFoodById.do";

//    //菜谱评价  getAllCommentsByFood.do
//    public static final String commentbufood = "getAllCommentsByFood.do";

    //收藏列表  getAllUserCollection.do
    public static final String usercollection = "getAllUserCollection.do";

    //收藏/取消收藏(店铺)   userCollectShop.do
    public static final String usercollectionshop = "userCollectShop.do";

    //收藏/取消收藏(菜谱)   userCollectFood.do
    public static final String usercollectionfood = "userCollectFood.do";

    //判断是否收藏   isCollected.do
    public static final String iscollect = "isCollected.do";

    //搜索菜谱/口味  getFoodBySearch.do
    public static final String foodbysearch = "getFoodBySearch.do";

    //获取用户信息   getUserById.do
    public static final String userbyid = "getUserById.do";

    //修改用户信息   updateUserById.do
    public static final String updateuserid = "updateUserById.do";

    //获取菜谱评价
    public static final String commentfood = "getAllCommentsByFood.do";
}
