package com.example.ALIENWARE.a16110100115gyh.bean;


public class CommentBean {

    /**
     * order_id : 1528082221311
     * item_id : 12
     * user_id : 1
     * food_id : 1
     * shop_id : 0
     * foodname : 酸菜鱼
     * shopname : 满口香川菜
     * price : 23
     * num : 3
     * username : null
     * order_time : 2018-06-04 00:00:00
     * sum : 23
     * suggesttime : 3:45----4:15
     * address : 北京
     * state : 0
     * content : 123457689ljp
     * comment_time : 2018-11-23
     */

    private long order_id;
    private int item_id;
    private int user_id;
    private int food_id;
    private int shop_id;
    private String foodname;
    private String shopname;
    private int price;
    private int num;
    private String username;
    private String order_time;
    private int sum;
    private String suggesttime;
    private String address;
    private int state;
    private String content;
    private String comment_time;

    public long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long order_id) {
        this.order_id = order_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getFood_id() {
        return food_id;
    }

    public void setFood_id(int food_id) {
        this.food_id = food_id;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getFoodname() {
        return foodname;
    }

    public void setFoodname(String foodname) {
        this.foodname = foodname;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public String getSuggesttime() {
        return suggesttime;
    }

    public void setSuggesttime(String suggesttime) {
        this.suggesttime = suggesttime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getComment_time() {
        return comment_time;
    }

    public void setComment_time(String comment_time) {
        this.comment_time = comment_time;
    }
}
