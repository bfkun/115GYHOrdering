package com.example.ALIENWARE.a16110100115gyh.bean;


public class UserbyidBean {

    /**
     * user_id : 1
     * username : lnn
     * userpass : e10adc3949ba59abbe56e057f20f883e
     * mobilenum : 13476543212
     * address : 北京
     * comment : 测试
     * img :
     */

    private int user_id;
    private String username;
    private String userpass;
    private String mobilenum;
    private String address;
    private String comment;
    private String img;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass;
    }

    public String getMobilenum() {
        return mobilenum;
    }

    public void setMobilenum(String mobilenum) {
        this.mobilenum = mobilenum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
