package com.example.ALIENWARE.a16110100115gyh.bean;


public class UsercollectBean {

    /**
     * user_id : 1
     * food_id : 0
     * shop_id : 1
     * collect_id : 53
     * username : lnn
     * foodname : null
     * shopname : 满口香川菜
     * flag : 0
     * pic : /images/shop/chuaicai.jpg
     * price : 0.0
     * address : 东软食堂三期
     */

    private int user_id;
    private int food_id;
    private int shop_id;
    private int collect_id;
    private String username;
    private Object foodname;
    private String shopname;
    private int flag;
    private String pic;
    private double price;
    private String address;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getFood_id() {
        return food_id;
    }

    public void setFood_id(int food_id) {
        this.food_id = food_id;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public int getCollect_id() {
        return collect_id;
    }

    public void setCollect_id(int collect_id) {
        this.collect_id = collect_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getFoodname() {
        return foodname;
    }

    public void setFoodname(Object foodname) {
        this.foodname = foodname;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
