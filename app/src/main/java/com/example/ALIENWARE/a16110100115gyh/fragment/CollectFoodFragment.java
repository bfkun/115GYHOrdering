package com.example.ALIENWARE.a16110100115gyh.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.activity.FoodbyidActivity;
import com.example.ALIENWARE.a16110100115gyh.adapter.CollectionFoodAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.BaseFragment;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.UsercollectBean;
import com.example.ALIENWARE.a16110100115gyh.util.SharedPreferencesUtils;

import java.util.List;


public class CollectFoodFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private List<UsercollectBean> beanList;
    private CollectionFoodAdapter collectionFoodAdapter;
    private LinearLayoutManager layoutManager;
    Context context;
    private SharedPreferencesUtils sharedPreferencesUtils;
    private int userid;
    CollectionFoodAdapter.OnItemClickListener onItemClickListener;

    @Override
    public int getLayoutFile() {
        return R.layout.fragment_collect_food;
    }

    @Override
    public void initView(View view) {

        recyclerView = (RecyclerView)view.findViewById(R.id.rv_collect_food);
        context = getContext();
        sharedPreferencesUtils = new SharedPreferencesUtils(context);
    }

    @Override
    public void initEvent() {
        collectionFoodAdapter = new CollectionFoodAdapter(context);
        recyclerView.setAdapter(collectionFoodAdapter);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(context,DividerItemDecoration.VERTICAL));
        userid = Integer.parseInt(sharedPreferencesUtils.readString("userid"));
        BaseModel<List<UsercollectBean>> baseModel = new BaseModel<>();
        baseModel.callEnqueue(baseModel.service.getusercollect(userid,1),baseListener);


        onItemClickListener = new CollectionFoodAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(),FoodbyidActivity.class);
                startActivity(intent);
            }
        };
        collectionFoodAdapter.setOnItemClickListener(onItemClickListener);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View view) {

    }

    BaseListener<List<UsercollectBean>> baseListener = new BaseListener<List<UsercollectBean>>() {
        @Override
        public void onResponse(List<UsercollectBean> usercollectBeans) {
            beanList = usercollectBeans;
            if (beanList == null){
                showToast("空");
            }else {
                collectionFoodAdapter.setList(beanList);
            }
        }

        @Override
        public void onFail(String msg) {

            showToast(msg);
        }
    };
}
