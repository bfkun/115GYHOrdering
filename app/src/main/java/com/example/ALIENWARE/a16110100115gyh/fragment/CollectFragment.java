package com.example.ALIENWARE.a16110100115gyh.fragment;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class CollectFragment extends BaseFragment {

    private ViewPager vp_collect;
    private TabLayout tab_collect;
    private String[] titles = {"商家","美食"};

    public CollectFragment(){

    }
    @Override
    public int getLayoutFile() {
        return R.layout.fragment_collect;
    }

    @Override
    public void initView(View view) {
        vp_collect = view.findViewById(R.id.vp_collection);
        tab_collect = view.findViewById(R.id.tab_collection);
    }

    @Override
    public void initEvent() {
        final List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new CollectShopFragment());
        fragmentList.add(new CollectFoodFragment());

        vp_collect.setAdapter(new FragmentPagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return titles[position];
            }
        });

        tab_collect.setupWithViewPager(vp_collect);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View view) {

    }
}
