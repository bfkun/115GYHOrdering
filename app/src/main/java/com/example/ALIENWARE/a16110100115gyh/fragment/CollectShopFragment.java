package com.example.ALIENWARE.a16110100115gyh.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.activity.FoodbyshopActivity;
import com.example.ALIENWARE.a16110100115gyh.adapter.CollectionShopAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.BaseFragment;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.UsercollectBean;
import com.example.ALIENWARE.a16110100115gyh.util.SharedPreferencesUtils;

import java.util.List;

public class CollectShopFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private List<UsercollectBean> beanList;
    private CollectionShopAdapter collectionShopAdapter;
    private LinearLayoutManager layoutManager;
    Context context;
    private SharedPreferencesUtils sharedPreferencesUtils;
    private int userid;
    CollectionShopAdapter.OnItemClickListener onItemClickListener;

    @Override
    public int getLayoutFile() {
        return R.layout.fragment_collect_shop;
    }

    @Override
    public void initView(View view) {

        recyclerView = (RecyclerView)view.findViewById(R.id.rv_collection_shop);
        context = getContext();
        sharedPreferencesUtils = new SharedPreferencesUtils(context);
    }

    @Override
    public void initEvent() {
        collectionShopAdapter = new CollectionShopAdapter(context);
        recyclerView.setAdapter(collectionShopAdapter);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(context,DividerItemDecoration.VERTICAL));
        userid = Integer.parseInt(sharedPreferencesUtils.readString("userid"));

        BaseModel<List<UsercollectBean>> baseModel = new BaseModel<>();
        baseModel.callEnqueue(baseModel.service.getusercollect(userid,0),baseListener);

        onItemClickListener = new CollectionShopAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(),FoodbyshopActivity.class);
                startActivity(intent);
            }
        };
        collectionShopAdapter.setOnItemClickListener(onItemClickListener);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View view) {

    }

    BaseListener<List<UsercollectBean>> baseListener = new BaseListener<List<UsercollectBean>>() {
        @Override
        public void onResponse(List<UsercollectBean> usercollectBeans) {
            beanList = usercollectBeans;
            if (beanList == null){
                showToast("空");
            }else {
                collectionShopAdapter.setList(beanList);
            }
        }

        @Override
        public void onFail(String msg) {

            showToast(msg);
        }
    };
}
