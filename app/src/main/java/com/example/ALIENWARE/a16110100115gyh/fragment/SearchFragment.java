package com.example.ALIENWARE.a16110100115gyh.fragment;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.adapter.SearchAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.BaseFragment;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.bean.FoodbysearchBean;
import com.example.ALIENWARE.a16110100115gyh.model.SearchModel;

import java.util.List;

public class SearchFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private EditText et_search;
    private Button bt_search_food;
    private List<FoodbysearchBean> beans;
    private SearchAdapter searchAdapter;
    private LinearLayoutManager layoutManager;
    Context context;
    @Override
    public int getLayoutFile() {
        return R.layout.fragment_search;
    }

    @Override
    public void initView(View view) {

        recyclerView = view.findViewById(R.id.rv_search);
        et_search = view.findViewById(R.id.et_search);
        bt_search_food = view.findViewById(R.id.bt_search_food);
        context = getContext();

        bt_search_food.setOnClickListener(this);
    }

    @Override
    public void initEvent() {

        searchAdapter = new SearchAdapter(context);
        recyclerView.setAdapter(searchAdapter);
        layoutManager = new LinearLayoutManager(context);   //实例化布局管理器
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);    //提高item性能
        recyclerView.addItemDecoration(new DividerItemDecoration(context,DividerItemDecoration.VERTICAL));
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_search_food:
                String search = et_search.getText().toString();
                SearchModel searchModel = new SearchModel();
                searchModel.getfoodsearch(search,listener);

        }
    }

    BaseListener<List<FoodbysearchBean>> listener = new BaseListener<List<FoodbysearchBean>>() {
        @Override
        public void onResponse(List<FoodbysearchBean> foodbysearchBeans) {
            beans = foodbysearchBeans;
            if (beans == null){
                showToast("空");
            }else {
                searchAdapter.setList(beans);
            }
        }

        @Override
        public void onFail(String msg) {
            showToast(msg);
        }
    };
}
