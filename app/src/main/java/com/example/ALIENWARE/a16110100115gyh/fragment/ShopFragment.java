package com.example.ALIENWARE.a16110100115gyh.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.activity.FoodbyshopActivity;
import com.example.ALIENWARE.a16110100115gyh.adapter.AllshopAdapter;
import com.example.ALIENWARE.a16110100115gyh.base.BaseFragment;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.bean.AllshopBean;
import com.example.ALIENWARE.a16110100115gyh.model.AllshopModel;
import com.example.ALIENWARE.a16110100115gyh.util.SharedPreferencesUtils;

import java.util.List;

public class ShopFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private List<AllshopBean> beans;
    private AllshopAdapter allshopAdapter;
    private LinearLayoutManager layoutManager;
    Context context;
    AllshopAdapter.OnItemClickListener onItemClickListener;
    private SharedPreferencesUtils sharedPreferencesUtils;
    @Override
    public int getLayoutFile() {
        return R.layout.fragment_shop;
    }

    @Override
    public void initView(View view) {
        recyclerView = (RecyclerView)view.findViewById(R.id.shopall_recycler);
       context = getContext();
       sharedPreferencesUtils = new SharedPreferencesUtils(context);
    }

    @Override
    public void initEvent() {
        allshopAdapter = new AllshopAdapter(context);
       recyclerView.setAdapter(allshopAdapter);
        layoutManager = new LinearLayoutManager(context);   //实例化布局管理器
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);    //提高item性能
        recyclerView.addItemDecoration(new DividerItemDecoration(context,DividerItemDecoration.VERTICAL));
        onItemClickListener = new AllshopAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                int shop_id = beans.get(position).getShop_id();
                Intent intent = new Intent(getActivity(),FoodbyshopActivity.class);
                intent.putExtra("shop_id",shop_id);
                startActivity(intent);
            }
        };
        allshopAdapter.setOnItemClickListener(onItemClickListener);

        AllshopModel allshopModel = new AllshopModel();
        allshopModel.getallshop(baseListener);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View view) {

    }

    BaseListener<List<AllshopBean>> baseListener = new BaseListener<List<AllshopBean>>() {
        @Override
        public void onResponse(List<AllshopBean> allshopBeans) {
            beans=allshopBeans;
            if (beans==null){
                Toast.makeText(getContext(),"Kong", Toast.LENGTH_LONG).show();

            }

            else{
                allshopAdapter.setList(beans);

            }
        }

        @Override
        public void onFail(String msg) {
            Toast.makeText(getContext(),msg, Toast.LENGTH_SHORT).show();
        }
    };
}
