package com.example.ALIENWARE.a16110100115gyh.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.example.ALIENWARE.a16110100115gyh.R;
import com.example.ALIENWARE.a16110100115gyh.activity.CommentActivity;
import com.example.ALIENWARE.a16110100115gyh.activity.OrderActivity;
import com.example.ALIENWARE.a16110100115gyh.activity.UpdateuserActivity;
import com.example.ALIENWARE.a16110100115gyh.base.BaseFragment;
import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.bean.UserbyidBean;
import com.example.ALIENWARE.a16110100115gyh.model.UserModel;
import com.example.ALIENWARE.a16110100115gyh.util.SharedPreferencesUtils;

public class UserInfoFragment extends BaseFragment {

    private TextView tv_user_mobile,tv_user_yonghuming;
    private TextView tv_user_xiugai , tv_user_comment , tv_user_dingdan;
    private SharedPreferencesUtils sharedPreferencesUtils;
    private Integer user_id;
    private UserbyidBean bean;

    @Override
    public int getLayoutFile() {
        return R.layout.fragment_user_info;
    }

    @Override
    public void initView(View view) {
        tv_user_mobile = view.findViewById(R.id.tv_user_mobile);
        tv_user_yonghuming = view.findViewById(R.id.tv_user_yonghuming);
        tv_user_xiugai = view.findViewById(R.id.tv_user_xiugai);
        tv_user_comment = view.findViewById(R.id.tv_user_gouwuche);
        tv_user_dingdan = view.findViewById(R.id.tv_user_dingdan);

        tv_user_xiugai.setOnClickListener(this);
        tv_user_comment.setOnClickListener(this);
        tv_user_dingdan.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    public void initEvent() {
        sharedPreferencesUtils = new SharedPreferencesUtils(getContext());
        user_id = Integer.parseInt(sharedPreferencesUtils.readString("userid"));

        UserModel userModel = new UserModel();
        userModel.getuserbyid(user_id , baseListener);
    }

    BaseListener<UserbyidBean> baseListener = new BaseListener<UserbyidBean>() {
        @Override
        public void onResponse(UserbyidBean userbyidBean) {
            if (userbyidBean != null){
                tv_user_yonghuming.setText(userbyidBean.getUserpass());
                tv_user_mobile.setText(userbyidBean.getMobilenum());
            }else {
                showToast("kong");
            }
        }

        @Override
        public void onFail(String msg) {
            showToast(msg);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_user_xiugai:
                Intent intent = new Intent(getActivity(),UpdateuserActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_user_gouwuche:
                Intent intent1 = new Intent(getActivity() , CommentActivity.class);
                startActivity(intent1);
                break;
            case R.id.tv_user_dingdan:
                Intent intent2 = new Intent(getActivity() , OrderActivity.class);
                startActivity(intent2);
                break;
        }
    }
}
