package com.example.ALIENWARE.a16110100115gyh.model;

import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.AllshopBean;

import java.util.List;


public class AllshopModel extends BaseModel<List<AllshopBean>> {

    public void getallshop(final BaseListener baseListener){
        call = service.getallshop();
        callEnqueue(call,baseListener);
    }
}
