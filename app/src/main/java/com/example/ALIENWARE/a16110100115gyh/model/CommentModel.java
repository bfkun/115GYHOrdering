package com.example.ALIENWARE.a16110100115gyh.model;

import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.CommentBean;

import java.util.List;

public class CommentModel extends BaseModel<List<CommentBean>> {

    public void getAllComments(int user_id , final BaseListener baseListener){
        call = service.getAllComments(user_id);
        callEnqueue(call , baseListener);
    }
}
