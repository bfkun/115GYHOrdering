package com.example.ALIENWARE.a16110100115gyh.model;

import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.FoodbyshopBean;

import java.util.List;


public class FoodbyshopModel extends BaseModel<List<FoodbyshopBean>> {
    public void getfoodbyshop(int shop_id, final BaseListener baseListener){
        call = service.getfoodbyshop(shop_id);
        callEnqueue(call,baseListener);
    }
}
