package com.example.ALIENWARE.a16110100115gyh.model;

import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.LoginBean;


public class LoginModel extends BaseModel<LoginBean> {

    public void login(String username, String userpass, final BaseListener listener){
        call = service.login(username,userpass);
        callEnqueue(call,listener);
    }
}
