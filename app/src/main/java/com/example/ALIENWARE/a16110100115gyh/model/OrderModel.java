package com.example.ALIENWARE.a16110100115gyh.model;

import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.CommentBean;

import java.util.List;

public class OrderModel extends BaseModel<List<CommentBean>> {

    public void getAllOrders(int user_id , final BaseListener baseListener){

        call = service.getAllOrders(user_id);
        callEnqueue(call , baseListener);
    }
}
