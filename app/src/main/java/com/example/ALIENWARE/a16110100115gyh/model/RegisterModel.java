package com.example.ALIENWARE.a16110100115gyh.model;


import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.RegisterBean;


public class RegisterModel extends BaseModel<RegisterBean> {
    public void register(String username, String userpass, String mobilenum, String address, String comment, final BaseListener baseListener){
        call = service.register(username,userpass,mobilenum,address,comment);
        callEnqueue(call,baseListener);
    }
}
