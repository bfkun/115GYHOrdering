package com.example.ALIENWARE.a16110100115gyh.model;

import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.FoodbysearchBean;

import java.util.List;

public class SearchModel extends BaseModel<List<FoodbysearchBean>> {

    public void getfoodsearch(String search , final BaseListener baseListener){

        call = service.getfoodsearch(search);
        callEnqueue(call , baseListener);
    }
}
