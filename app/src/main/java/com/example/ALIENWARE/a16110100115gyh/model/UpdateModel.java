package com.example.ALIENWARE.a16110100115gyh.model;

import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.UpdateuserBean;

public class UpdateModel extends BaseModel<UpdateuserBean> {
    public void getupdateuser(int user_id , String username , String userpass , String mobilenum , String address , final BaseListener baseListener){
        call = service.getupdateuser(user_id , username , userpass , mobilenum , address);
        callEnqueue(call , baseListener);
    }
}
