package com.example.ALIENWARE.a16110100115gyh.model;


import com.example.ALIENWARE.a16110100115gyh.base.BaseListener;
import com.example.ALIENWARE.a16110100115gyh.base.BaseModel;
import com.example.ALIENWARE.a16110100115gyh.bean.UserbyidBean;

public class UserModel extends BaseModel<UserbyidBean> {

    public void getuserbyid(int user_id , final BaseListener baseListener){
        call = service.getuserbyid(user_id);
        callEnqueue(call,baseListener);
    }
}
