package com.example.ALIENWARE.a16110100115gyh.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtils {
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    public SharedPreferencesUtils(Context context) {
        sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public boolean saveString(String key, String value) {
        editor.putString(key, value);
        return editor.commit();
    }

    public String readString(String key) {
        String str = null;
        str = sp.getString(key, null);
        return str;
    }
    public void clearSp(){
        editor.clear();
        editor.commit();
    }

}
